import React, { useEffect } from "react";

function Carousel() {
  useEffect(() => {
    if (window.loadOwlCarousel) {
      window.loadOwlCarousel();
    }
    if (document.querySelector(".owl-prev")) {
      const prev = document.querySelector(".owl-prev");
      prev.innerHTML = "<img src='/images/Left-button.svg' alt='' />";
    }
    if (document.querySelector(".owl-next")) {
      const next = document.querySelector(".owl-next");
      next.innerHTML = "<img src='/images/Right-button.svg' alt='' />";
    }
  }, []);

  return (
    <>
      <div className="testimonial" id="testimonial">
        <h2>Testimonial</h2>
        <p>Berbagai review positif dari para pelanggan kami</p>
      </div>

      <div id="owl-container">
        <div className="owl-carousel owl-theme">
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo2.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo2.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo2.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo2.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
          <div className="item carousel-testi">
            <div className="inside-card row">
              <div className="col-md-3 img-testi">
                <img src="./images/img_photo2.svg" alt="..." />
              </div>
              <div className="col-md-9 text-testi">
                <div className="card-body">
                  <img src="./images/Rate.svg" alt="" className="rate" />
                  <p className="card-text">
                    "Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Praesentium dicta, voluptate consectetur reprehenderit nulla
                    provident quasi ipsum eaque ipsam quam cum neque ipsa
                    aliquid, Lorem ipsum dolor sit amet.
                  </p>
                  <p className="name-testi">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Carousel;
