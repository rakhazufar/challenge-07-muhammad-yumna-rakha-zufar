import React from "react";

function WhyUs() {
  return (
    <div className="why-us" id="why-us">
      <div className="container-why-us">
        <h2>Why Us?</h2>
        <p>Mengapa harus memilih Binar Car Rental</p>
        <div className="list-item">
          <div className="item">
            <img src="/images/thumbs.svg" alt="thumb" />
            <h3>Mobil Lengkap</h3>
            <p>
              Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
              terawat
            </p>
          </div>
          <div className="item">
            <img src="/images/icon_price.svg" alt="thumb" />
            <h3>Harga Murah</h3>
            <p>
              Harga murah dan bersaing, bisa bandingkan harga kami dengan rental
              mobil lain
            </p>
          </div>
          <div className="item">
            <img src="/images/icon_24hrs.svg" alt="thumb" />
            <h3>Layanan 24 Jam</h3>
            <p>
              Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
              tersedia di akhir minggu
            </p>
          </div>
          <div className="item">
            <img src="/images/icon_professional.svg" alt="thumb" />
            <h3>Sopir Professional</h3>
            <p>
              Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
              tepat waktu
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default WhyUs;
