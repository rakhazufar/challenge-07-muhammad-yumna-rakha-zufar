import { gapi } from "gapi-script";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Login from "./Login";
import Logout from "./Logout";

const Navbar = () => {
  const clientId =
    "360574061073-0cbns0gf0281h4qc6fv2d4eidcet1ah0.apps.googleusercontent.com";
  const [isLogin, setIsLogin] = useState(false);
  const [users, setUsers] = useState({});

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    }

    gapi.load("client:auth2", start);
  });

  return (
    <>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link to="/" className="navbar-brand">
            <img src="/images/Rectangle.svg" alt="" />
          </Link>

          <button
            className="btn toggle-sidebar"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasRight"
            aria-controls="offcanvasRight"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="/#our-services"
                >
                  Our Services
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#why-us">
                  Why Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#testimonial">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#FAQ">
                  FAQ
                </a>
              </li>

              <li className="nav-item button-login">
                {isLogin === true ? (
                  <div className="user">
                    <p>Hallo, {users.name} </p>
                    <Logout isLogin={setIsLogin} />
                  </div>
                ) : (
                  <Login isLogin={setIsLogin} userData={setUsers} />
                )}
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div
        className="offcanvas offcanvas-end"
        tabIndex="-1"
        id="offcanvasRight"
        aria-labelledby="offcanvasRightLabel"
      >
        <div className="offcanvas-header">
          <h5 id="offcanvasRightLabel">BCR</h5>
          <button
            type="button"
            className="btn-close text-reset"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          ></button>
        </div>
        <div className="offcanvas-body">
          <div className="navbar-nav">
            <li className="nav-item">
              <a
                className="nav-link active"
                aria-current="page"
                href="/#our-services"
              >
                Our Services
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/#why-us">
                Why Us
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/#testimonial">
                Testimonial
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/#FAQ">
                FAQ
              </a>
            </li>
            <li className="nav-item button-login">
              {isLogin === true ? (
                <div className="user">
                  <p>Hallo, {users.name} </p>
                  <Logout isLogin={setIsLogin} />
                </div>
              ) : (
                <Login isLogin={setIsLogin} userData={setUsers} />
              )}
            </li>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
