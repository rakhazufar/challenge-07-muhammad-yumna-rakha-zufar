import React from "react";
import { GoogleLogout } from "react-google-login";

function Logout({ isLogin }) {
  const clientId =
    "360574061073-0cbns0gf0281h4qc6fv2d4eidcet1ah0.apps.googleusercontent.com";
  function onSuccess() {
    isLogin(false);
  }
  return (
    <div>
      <GoogleLogout
        clientId={clientId}
        render={(renderProps) => (
          <button
            onClick={renderProps.onClick}
            className="btn btn-success"
            disabled={renderProps.disabled}
          >
            Logout
          </button>
        )}
        buttonText="Logout"
        onLogoutSuccess={onSuccess}
      ></GoogleLogout>
    </div>
  );
}

export default Logout;
