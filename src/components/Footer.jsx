const Footer = () => {
  return (
    <footer>
      <div className="container-footer">
        <div className="address">
          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
          <p>binarcarrental@gmail.com</p>
          <p>081-233-334-808</p>
        </div>
        <div className="button">
          <p>Our Services</p>
          <p>Why Us</p>
          <p>Testimonial</p>
          <p>FAQ</p>
        </div>
        <div className="social-media">
          <p>Connect with us</p>
          <img src="/images/list-item.svg" alt="" />
        </div>
        <div className="copyright">
          <p>Copyright Binar 2022</p>
          <img src="/images/Rectangle.svg" alt="" />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
