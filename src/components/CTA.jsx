import React from "react";

function CTA() {
  return (
    <div className="container-CTA">
      <div className="CTA">
        <h2>Sewa Mobil di (Lokasimu) Sekarang</h2>
        <p>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nam corporis
          molestias placeat sint expedita ullam? Lorem ipsum dolor sit amet.
        </p>
        <button className="btn btn-success">Mulai Sewa Mobil</button>
      </div>
    </div>
  );
}

export default CTA;
