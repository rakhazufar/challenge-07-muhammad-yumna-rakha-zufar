import React from "react";

function OurServices() {
  return (
    <div id="our-services" className="our-services">
      <div className="image-services">
        <img id="girl" src="/images/img_service.svg" alt="girl" />
      </div>

      <div className="services">
        <div className="container-services">
          <h2>Best Car Rental for any kind of trip in Bogor!</h2>
          <p>
            Sewa mobil di Bogor bersama Binar Car Rental jaminan harga lebih
            murah dibandingkan yang lain, kondisi mobil baru, serta kualitas
            pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting,
            dll.
          </p>
          <div className="check-services">
            <img src="/images/check.svg" alt="" />
            <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
          </div>
          <div className="check-services">
            <img src="/images/check.svg" alt="" />
            <p>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
          </div>
          <div className="check-services">
            <img src="/images/check.svg" alt="" />
            <p>Sewa Mobil Jangka Panjang Bulanan</p>
          </div>
          <div className="check-services">
            <img src="/images/check.svg" alt="" />
            <p>Gratis Antar - Jemput Mobil di Bandara</p>
          </div>
          <div className="check-services">
            <img src="/images/check.svg" alt="" />
            <p>Layanan Airport Transfer / Drop In Out</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default OurServices;
