import React from "react";

function CarResults({ dataCars }) {
  console.log(dataCars);
  return (
    <>
      <div className="result-container">
        <div className="cards">
          {dataCars &&
            dataCars.map((car) => {
              return (
                <div key={car.id} className="col-md-2">
                  <img
                    src={car.image}
                    style={{ width: "100%" }}
                    alt={car.plate}
                  />
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
}

export default CarResults;
