import { useEffect, useRef } from "react";
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

const Header = () => {
  const location = useLocation();
  const ref = useRef();
  const addclass = () => {
    const button = ref.current;
    button.className = "hidden";
  };
  useEffect(() => {
    if (location.pathname === "/cars") {
      addclass();
    }
  }, []);

  return (
    <div className="hero">
      <div className="left-hero">
        <div className="container-hero">
          <h1>Sewa & Rental Mobil Terbaik di kawasan Bogor</h1>
          <p>
            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu
            untuk sewa mobil selama 24 jam.
          </p>
          <Link to="/cars" ref={ref} className="btn btn-success">
            Mulai Sewa Mobil
          </Link>
        </div>
      </div>

      <div className="right-hero">
        <div className="car-background"></div>
        <img src="./images/mobil.svg" alt="Merchedes" />
      </div>
    </div>
  );
};

export default Header;
