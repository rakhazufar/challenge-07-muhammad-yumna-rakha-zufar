import React, { useState } from "react";

function SearchBox({ showCar }) {
  const [filter, setFilter] = useState({
    tipedriver: "",
    date: "",
    waktujemput: "",
    capacity: "",
  });

  function changeTanggal() {
    const date = document.querySelector(".datepicker");
    const tanggal = document.querySelector(".tanggal");
    tanggal.innerHTML = date.value;
    tanggal.value = date.value;
    const newTanggal = new Date(tanggal.value);
    const tempData = newTanggal.getTime();
    setFilter({ ...filter, date: tempData });
  }

  function selectWaktu() {
    const waktu = document.querySelector(".select-waktu");
    setFilter({ ...filter, waktujemput: waktu.value * 3600 });
  }

  function setCapacity() {
    const capacity = document.querySelector(".capacity");
    setFilter({ ...filter, capacity: parseInt(capacity.value) });
  }

  function isDriver() {
    const driver = document.querySelector(".driver");
    if (driver.value === "true") {
      setFilter({ ...filter, tipedriver: true });
    }

    if (driver.value === "false") {
      setFilter({ ...filter, tipedriver: false });
    }
  }

  return (
    <>
      <div className="row">
        <div className="container-searchBox">
          <div className="searchBox">
            <div className="searchBoxItem">
              <label htmlFor="validationCustom04" className="form-label">
                Tipe Driver
              </label>
              <select
                className="form-select search-car driver"
                id="validationCustom04"
                required
                onChange={isDriver}
              >
                <option selected disabled value="">
                  Pilih Tipe Driver
                </option>
                <option value={true}>Dengan Supir</option>
                <option value={false}>Tanpa Supir (Lepas Kunci)</option>
              </select>
            </div>
            <div className="searchBoxItem" style={{ position: "relative" }}>
              <label htmlFor="validationCustom04" className="form-label">
                Tanggal
              </label>
              <img src="./images/date.webp" className="datepick" alt="" />
              <div className="datepicker-container">
                <input
                  type="date"
                  className="datepicker search-car"
                  onChange={() => changeTanggal()}
                />
              </div>

              <select
                className="form-select form-tanggal"
                id="validationCustom04"
                aria-placeholder="Pick"
                disabled
              >
                <option selected disabled value="" className="tanggal">
                  Pilih Tanggal
                </option>
              </select>
            </div>
            <div className="searchBoxItem" style={{ position: "relative" }}>
              <label htmlFor="validationCustom04" className="form-label">
                Waktu Jemput/Ambil
              </label>
              <img src="./images/download.png" alt="" className="timepick" />
              <select
                className="form-select search-car select-waktu"
                id="validationCustom04"
                onChange={selectWaktu}
              >
                <option selected value="0">
                  Pilih Waktu
                </option>
                <option value="8">08.00 WIB</option>
                <option value="9">09.00 WIB</option>
                <option value="10">10.00 WIB</option>
                <option value="11">11.00 WIB</option>
                <option value="12">12.00 WIB</option>
              </select>
            </div>
            <div className="searchBoxItem" style={{ position: "relative" }}>
              <label>Jumlah Penumpang(optional)</label>
              <div
                className="input-jumlah-penumpang search-car"
                style={{ display: "flex" }}
              >
                <input
                  type="text"
                  className="form-control capacity"
                  placeholder="Jumlah penumpang"
                  aria-describedby="basic-addon2"
                  onChange={setCapacity}
                />
                <span className="capacitypick">
                  <img style={{ width: 20 }} src="./images/users.png" alt="" />
                </span>
              </div>
            </div>
            <div className="searchBoxButton">
              <button
                className="btn btn-success"
                onClick={() => showCar(filter)}
              >
                Cari Mobil
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SearchBox;
