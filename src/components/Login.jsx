import React, { useState } from "react";
import { GoogleLogin } from "react-google-login";

function Login({ isLogin, userData }) {
  const clientId =
    "360574061073-0cbns0gf0281h4qc6fv2d4eidcet1ah0.apps.googleusercontent.com";
  function onSuccess(res) {
    userData(res.profileObj);
    isLogin(true);
  }
  function onFailure(res) {
    console.log(res);
  }
  return (
    <div>
      <GoogleLogin
        clientId={clientId}
        render={(renderProps) => (
          <button
            onClick={renderProps.onClick}
            className="btn btn-success"
            disabled={renderProps.disabled}
          >
            Login with Google
          </button>
        )}
        buttonText="Login"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
        isSignedIn={true}
      />
    </div>
  );
}

export default Login;
