import { useState } from "react";
import SearchBox from "../components/SearchBox";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getAsyncData } from "../reducers/api-store";

const Cars = () => {
  const dispatch = useDispatch();
  const listCarsJson = useSelector((state) => state.api.cars);
  const [dataCar, setDataCar] = useState([]);
  useEffect(() => {
    dispatch(getAsyncData());
  }, [dispatch]);

  const showCar = (filter) => {
    let tempData = [];
    setDataCar([]);
    listCarsJson.map((car) => {
      let date = new Date(car.availableAt);
      if (
        car.capacity >= filter.capacity &&
        filter.waktujemput + filter.date > date &&
        car.available === true
      ) {
        tempData.push(car);
      }
    });
    setDataCar(tempData);
  };
  return (
    <>
      <SearchBox showCar={showCar} />
      <div className="result-container">
        <div className="cards">
          {dataCar &&
            dataCar.map((car) => {
              return (
                <div className="card" style={{ width: "18rem" }} key={car.id}>
                  <img
                    className="card-img-top"
                    src={car.image}
                    alt="Card image cap"
                  />
                  <div className="card-body">
                    <h5 className="card-title">
                      {car.manufacture} {car.model}
                    </h5>
                    <p className="card-text rentperday">
                      Rp. {car.rentPerDay} / hari
                    </p>
                    <p className="card-text">{car.description}</p>
                    <p className="card-text">
                      <img
                        style={{ width: 20, height: 20 }}
                        src="/images/users.png"
                        alt=""
                      />{" "}
                      {car.capacity} orang
                    </p>
                    <p className="card-text">
                      <img
                        style={{ width: 20, height: 20 }}
                        src="./images/settings.png"
                        alt=""
                      />{" "}
                      {car.transmission}
                    </p>
                    <p className="card-text">
                      <img
                        style={{ width: 20, height: 20 }}
                        src="/images/fi_calendar.svg"
                        alt=""
                      />{" "}
                      {car.year}
                    </p>

                    <a href="/" className="btn btn-success btn-card">
                      Pilih Mobil
                    </a>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
};

export default Cars;
