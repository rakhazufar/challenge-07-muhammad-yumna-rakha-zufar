import OurServices from "../components/OurServices";
import WhyUs from "../components/WhyUs";
import Carousel from "../components/Carousel";
import CTA from "../components/CTA";
import FAQ from "../components/FAQ";

const Home = () => {
  return (
    <>
      <OurServices />
      <WhyUs />
      <Carousel />
      <CTA />
      <FAQ />
    </>
  );
};

export default Home;
