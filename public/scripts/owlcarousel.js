function loadOwlCarousel() {
  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
      0: {
        items: 1,
      },
      992: {
        items: 3,
      },
    },
  });
}
